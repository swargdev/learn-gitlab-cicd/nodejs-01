let express = require('express')
let path = require('path')
let fs = require('fs')
let app = express()

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'index.html'))
})

app.get('/profile-img/alice', function (req, res) {
  let img = fs.readFileSync(path.join(__dirname, 'images/profile-alice.jpg'))
  res.writeHead(200, { 'Content-Type': 'image/jpg' })
  res.end(img, 'binary')
})

app.get('/profile-img/char', function (req, res) {
  let img = fs.readFileSync(path.join(__dirname, 'images/profile-char.jpg'))
  res.writeHead(200, { 'Content-Type': 'image/jpg' })
  res.end(img, 'binary')
})

app.listen(8000, function () {
  console.log('app listening on port 8000')
})
